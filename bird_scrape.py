from bs4 import BeautifulSoup
import requests
import json

BIRD_URL = "https://www.bto.org/understanding-birds/birdfacts/british-list"

birds = []


def get_content(url):
    return requests.get(url).text


soup = BeautifulSoup(get_content(BIRD_URL), "html.parser")

# The table isn't named :( but it's the only table so we'll assume a few things
bird_table = soup.table
column_names = [
    "common_name",
    "sci_name",
    "status",
    "population",
    "two_letter",
    "five_letter",
]

table_rows = bird_table.find_all("tr")

for bird_row in table_rows:
    columns = bird_row.find_all("td")
    birds.append(
        {column_names[index]: column.text for index, column in enumerate(columns)}
    )

with open("static/birds.json", "w") as f:
    f.write(json.dumps(birds, indent=4))
