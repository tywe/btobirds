# importing Flask and other modules
from flask import Flask, request, render_template
from bird_lookup import find_bird,fuzzy_find_birds

app = Flask(__name__)

@app.route('/', methods =["GET", "POST"])
def birdie():
    if request.method == "POST":
        search = request.form.get("search")
        if not search or search.strip() == "":
            return render_template("index.html", results="", value = "")
        else:
            search = search.strip()
            if request.form['button'] == "search":
                bird_data = find_bird(search)
            else:
                bird_data = fuzzy_find_birds(search)
            return render_template("index.html", results=bird_data, value=search)
    return render_template("index.html", results="", value = "")


if __name__=='__main__':
    from waitress import serve
    serve(app, host="0.0.0.0", port=8080)