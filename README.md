# BTO birds

Demo: [https://birds.fly.dev](https://birds.fly.dev)

## What?

Quick ref for this list of British bird info from BTO: https://www.bto.org/understanding-birds/birdfacts/british-list

## Why?

https://mobile.twitter.com/sarahdal/status/1575770907582275593

## How?

### Setup

```bash
# install pipenv
pip install --user pipenv

# install requirements into pipenv
pipenv install

# run virtual env
pipenv shell
```

### Optional - Update bird information

```bash
# while within pipenv shell
python bird_scrape.py
```

### Run locally

```bash
# while within pipenv shell
python birds.py
```

Then navigate to http://localhost:8080

### Deploy with Fly

#### Mac (haven't tried on Windows yet)

```bash
brew install flyctl

flyctl auth login
flyctl deploy
```
