import json

# lookup_value = "si"


def load_birds(filename="static/birds.json"):
    with open(filename, "r") as f:
        return json.loads(f.read())


birds = load_birds()


def find_bird(search):
    for bird in birds:
        bird_lowercase_info = [value.casefold() for value in bird.values()]
        # Check if search matches an existing value
        if search.casefold() in bird_lowercase_info:
            return [bird]
    return []


def fuzzy_find_birds(search):
    matches = []
    for bird in birds:
        bird_lowercase_info = [value.casefold() for value in bird.values()]
        # Check if search is part of any of the existing values
        for value in bird_lowercase_info:
            if search.casefold() in value:
                matches.append(bird)
                break
    return matches



def convert_results(results_json):
    results_html = ""
    for key, value in results_json.items():
        results_html += f'{key} <input type="text" value="{value}" readonly><br />'
    return results_html
